<?php
/**
 * Your code here.
 *
 */


// Giving Editors Access to Gravity Forms
function add_grav_forms() {
	$role = get_role('editor');
	$role->add_cap('gform_full_access');
}
add_action('admin_init','add_grav_forms');


// gravity form visibility of the Field Label
add_filter('gform_enable_field_label_visibility_settings', '__return_true');
